import base64
import json
import os
import time
import yaml
from pathlib import Path

from google.cloud.storage import Client, transfer_manager
from google.auth.exceptions import GoogleAuthError

from bitbucket_pipes_toolkit import Pipe


schema = {
    'KEY_FILE': {'type': 'string', 'required': True},
    'BUCKET': {'type': 'string', 'required': True},
    'SOURCE': {'type': 'string', 'required': True},
    'BLOB_NAME_PREFIX': {'type': 'string', 'required': False, 'default': ''},
    'CACHE_CONTROL': {'type': 'string', 'required': False},
    'CONTENT_DISPOSITION': {'type': 'string', 'required': False},
    'CONTENT_ENCODING': {'type': 'string', 'required': False},
    'CONTENT_LANGUAGE': {'type': 'string', 'required': False},
    'CONTENT_TYPE': {'type': 'string', 'required': False},
    'ACL': {'type': 'string', 'required': False},
    'STORAGE_CLASS': {'type': 'string', 'required': False},
    'EMPTY_BUCKET': {'type': 'boolean', 'required': False, 'default': False},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


class GoogleCloudStorageDeploy(Pipe):
    OIDC_AUTH = 'OIDC_AUTH'
    DEFAULT_AUTH = 'DEFAULT_AUTH'

    def __init__(self, *args, **kwargs):
        self.auth_method = self.discover_auth_method()
        super().__init__(*args, **kwargs)
        self.bucket = self.get_variable('BUCKET')
        self.source = self.get_variable('SOURCE')
        self.blob_name_prefix = self.get_variable('BLOB_NAME_PREFIX')
        self.empty_bucket = self.get_variable('EMPTY_BUCKET')

    def discover_auth_method(self):
        """Discover user intentions: authenticate to GCloud through OIDC or default key file."""
        oidc_file = os.getenv('GOOGLE_OIDC_CONFIG_FILE')
        web_identity_token = os.getenv('BITBUCKET_STEP_OIDC_TOKEN')
        if oidc_file:
            if web_identity_token:
                schema['BITBUCKET_STEP_OIDC_TOKEN'] = {'required': True}
                schema['GOOGLE_OIDC_CONFIG_FILE'] = {'required': True}
                schema['PROJECT'] = {'required': True}

                schema['KEY_FILE']['required'] = False

                os.environ.pop('KEY_FILE', None)

                return self.OIDC_AUTH

            return self.DEFAULT_AUTH

        return self.DEFAULT_AUTH

    def auth(self):
        """Authenticate via chosen method"""
        if self.auth_method == self.OIDC_AUTH:
            self.log_info('Authenticating with a OpenID Connect (OIDC) Web Identity Provider.')
            self.parse_config(self.get_variable('GOOGLE_OIDC_CONFIG_FILE'), add_bitbucket_step_oidc_token=True)

            os.environ['GOOGLE_CLOUD_PROJECT'] = self.get_variable('PROJECT')

            self.log_debug('Configured settings for authentication with oidc.')
        elif self.auth_method == self.DEFAULT_AUTH:
            if os.getenv('GOOGLE_OIDC_CONFIG_FILE'):
                self.log_warning('Parameter "oidc: true" in the step configuration is required for OIDC authentication')

            self.log_info('Using default authentication with KEY_FILE.')
            self.parse_config(self.get_variable('KEY_FILE'))
        else:
            self.fail("Auth method not supported.")

    def parse_config(self, config_encoded_string, add_bitbucket_step_oidc_token=False):
        random_number = str(time.time_ns())
        config_dir = 'tmp'
        os.makedirs(config_dir, exist_ok=True)
        config_file_location = os.path.join(config_dir, f'file_{random_number}.json')

        key_content = base64.b64decode(config_encoded_string)
        decoded_key_content = key_content.decode()
        with open(config_file_location, 'w') as config_file:
            config_file.write(decoded_key_content)

        os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config_file_location

        if add_bitbucket_step_oidc_token:
            try:
                bitbucket_step_oidc_token_file_path = json.loads(decoded_key_content)['credential_source']['file']

                with open(bitbucket_step_oidc_token_file_path, 'w') as bitbucket_step_oidc_token_file:
                    bitbucket_step_oidc_token_file.write(os.getenv('BITBUCKET_STEP_OIDC_TOKEN'))
            except (TypeError, KeyError):
                self.fail(
                    'Failed to parse GOOGLE_OIDC_CONFIG_FILE. File should contain credential_source.file path'
                    ' to bitbucket-step-oidc-token.'
                )

    def run(self):
        self.auth()

        self.log_info("Starting deployment to GCP storage...")

        try:
            storage_client = Client()
        except GoogleAuthError as e:
            self.fail(f'Deployment failed. Reason: {e}.')

        bucket = storage_client.bucket(self.bucket)

        if self.empty_bucket:
            self.perform_emptying_bucket(bucket)

        if os.path.isdir(self.source):
            self.upload_directory_with_transfer_manager(bucket)
        elif os.path.isfile(self.source):
            self.upload_file(bucket)
        else:
            self.fail(
                f'Deployment failed. Unrecognized source path: {self.source}.'
                f' Should be a path to existing directory or file.'
            )

        self.success('Deployment successful.')

    @staticmethod
    def perform_emptying_bucket(bucket):
        # List all objects in the bucket
        blobs = bucket.list_blobs()
        for blob in blobs:
            blob.delete()

    def upload_directory_with_transfer_manager(self, bucket):
        """Upload every file in a directory, including all files in subdirectories.

        Each blob name is derived from the filename, not including the `directory`
        parameter itself. For complete control of the blob name for each file (and
        other aspects of individual blob metadata), use
        transfer_manager.upload_many() instead.
        """
        # Generate a list of paths (in string form) relative to the `directory`.
        # This can be done in a single list comprehension, but is expanded into
        # multiple lines here for clarity.

        # First, recursively get all files in `directory` as Path objects.
        directory_as_path_obj = Path(self.source)
        paths = directory_as_path_obj.rglob("*")

        # Filter so the list only includes files, not directories themselves.
        file_paths = [path for path in paths if path.is_file()]

        # These paths are relative to the current working directory. Next, make them
        # relative to `directory`
        relative_paths = [path.relative_to(self.source) for path in file_paths]

        # Finally, convert them all to strings.
        string_paths = [str(path) for path in relative_paths]

        self.log_debug(f"Found {len(string_paths)} files.")

        additional_blob_attributes = self.add_blob_attributes()

        upload_kwargs = None
        if self.get_variable('ACL'):
            upload_kwargs = {'predefined_acl': self.get_variable('ACL')}

        # Start the upload.
        transfer_manager.upload_many_from_filenames(
            bucket,
            string_paths,
            source_directory=self.source,
            blob_name_prefix=self.blob_name_prefix,
            additional_blob_attributes=additional_blob_attributes,
            upload_kwargs=upload_kwargs
        )

    def add_blob_attributes(self):
        attributes = dict()

        if self.get_variable('CACHE_CONTROL'):
            attributes['cache_control'] = self.get_variable('CACHE_CONTROL')

        if self.get_variable('CONTENT_DISPOSITION'):
            attributes['content_disposition'] = self.get_variable('CONTENT_DISPOSITION')

        if self.get_variable('CONTENT_ENCODING'):
            attributes['content_encoding'] = self.get_variable('CONTENT_ENCODING')

        if self.get_variable('CONTENT_LANGUAGE'):
            attributes['content_language'] = self.get_variable('CONTENT_LANGUAGE')

        if self.get_variable('CONTENT_TYPE'):
            attributes['content_type'] = self.get_variable('CONTENT_TYPE')

        if self.get_variable('STORAGE_CLASS'):
            attributes['storage_class'] = self.get_variable('STORAGE_CLASS')

        return attributes if attributes else None

    def upload_file(self, bucket):
        """Uploads a file to the bucket."""
        blob_name = f"{self.blob_name_prefix}{self.source}" if self.blob_name_prefix else self.source
        blob = bucket.blob(blob_name)

        self.update_blob_attributes(blob)

        blob.upload_from_filename(self.source, predefined_acl=self.get_variable('ACL'))

    def update_blob_attributes(self, blob):
        if self.get_variable('CACHE_CONTROL'):
            blob.cache_control = self.get_variable('CACHE_CONTROL')

        if self.get_variable('CONTENT_DISPOSITION'):
            blob.content_disposition = self.get_variable('CONTENT_DISPOSITION')

        if self.get_variable('CONTENT_ENCODING'):
            blob.content_encoding = self.get_variable('CONTENT_ENCODING')

        if self.get_variable('CONTENT_LANGUAGE'):
            blob.content_language = self.get_variable('CONTENT_LANGUAGE')

        if self.get_variable('CONTENT_TYPE'):
            blob.content_type = self.get_variable('CONTENT_TYPE')

        if self.get_variable('STORAGE_CLASS'):
            blob.storage_class = self.get_variable('STORAGE_CLASS')


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = GoogleCloudStorageDeploy(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()

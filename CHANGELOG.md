# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.2.0

- minor: Add support to empty bucket before deploy.
- patch: Internal maintenance: Bump packages in requirements.
- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.
- patch: Internal maintenance: Update links in README.

## 2.1.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 2.1.0

- minor: Add support of BLOB_NAME_PREFIX variable that allows to upload to custom destination blob.
- patch: Internal maintenance: Bump packages in requirements.
- patch: Internal maintenance: Bump pipes versions in pipelines configuration file.

## 2.0.0

- major: Refactor logic to python base. Implement OIDC support. Breaking change! Take a look at the README variables section for more information.

## 1.5.1

- patch: Internal maintenance: Bump pipes versions in pipelines config file.

## 1.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.4.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 1.3.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 1.2.0

- minor: Bump cloud-sdk image version to 409.0.0-alpine.
- minor: Fix bug with debug usage in gsutil command.

## 1.1.0

- minor: Update cloud-sdk docker image to 402.0.0.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update release process.

## 1.0.1

- patch: Internal maintenance: Bump bitbucket-pipes-toolkit -> 2.2.0.

## 1.0.0

- major: Update google cloud image to 342.0.0 (latest stable).

## 0.4.6

- patch: Fix bug with wildcard in the SOURCE variable.

## 0.4.5

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.4.4

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.4.3

- patch: Update README: clear description for base64 encoded environment variables

## 0.4.2

- patch: Internal maintenance: Add auto infrastructure for tests.
- patch: Internal maintenance: Update pipe release process.

## 0.4.1

- patch: Internal maintenance: Add gitignore secrets.

## 0.4.0

- minor: Bump google/cloud-sdk version to 286.0.0.

## 0.3.8

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.7

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.6

- patch: Updated contributing guidelines

## 0.3.5

- patch: Prevent pipe from hanging on error.

## 0.3.4

- patch: Standardising README and pipes.yml.

## 0.3.3

- patch: Added support for multiline keys files

## 0.3.2

- patch: Fixed support multi threaded directory uploads.

## 0.3.1

- patch: Fix typo in markdown link reference.

## 0.3.0

- minor: Add support for the DEBUG variable.
- minor: Switch naming conventions from task to pipes.

## 0.2.2

- patch: Use quotes for all pipes examples in README.md.

## 0.2.1

- patch: Restructure README.md to match user flow.

## 0.2.0

- minor: Modified gcloud commands to always use --quiet, to allow fast erroring and use of default settings.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines GCP storage deploy pipe.

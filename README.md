# Bitbucket Pipelines Pipe: Google Cloud Storage Deploy

Pipe to deploy to [Google Cloud Storage][Google Cloud Storage].
Copy files and directories. Recursively copies new and updated files from the source local directory to the destination. Only creates folders in the destination if they contain one or more files.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/google-cloud-storage-deploy:2.2.0
  variables:
    KEY_FILE: '<string>'
    BUCKET: '<string>'
    SOURCE: '<string>'
    # GOOGLE_OIDC_CONFIG_FILE: "<string>" # Optional by default. Required for OpenID Connect (OIDC) authentication.
    # PROJECT: "<string>" # Optional by default. Required with GOOGLE_OIDC_CONFIG_FILE.
    # CACHE_CONTROL: '<string>' # Optional.
    # CONTENT_DISPOSITION: '<string>' # Optional.
    # CONTENT_ENCODING: '<string>' # Optional.
    # CONTENT_LANGUAGE: '<string>' # Optional.
    # CONTENT_TYPE: '<string>' # Optional.
    # ACL: '<string>' # Optional.
    # STORAGE_CLASS: '<string>' # Optional.
    # EMPTY_BUCKET: '<boolean>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```

## Variables

| Variable                | Usage                                                                                                                                                                                                                                   |
|-------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| KEY_FILE (*)            | base64 encoded content of Key file for a [Google service account][Google service account]. To encode this content, follow [encode private key doc][encode-string-to-base64].                                                            |
| BUCKET (*)              | Google Cloud Storage bucket name.                                                                                                                                                                                                       |
| SOURCE (*)              | The source of the files to upload (can be a path to a local file or directory)                                                                                                                                                          |
| BLOB_NAME_PREFIX        | A string that will be prepended to each filename, in order to determine the name of the destination blob.                                                                                                                               |
| GOOGLE_OIDC_CONFIG_FILE | base64 encoded content of google oidc config file for [OIDC authentication][OIDC authentication]. To encode this content, follow [encode private key doc][encode-string-to-base64]. Option to authenticate instead of `KEY_FILE` usage. |
| PROJECT                 | The project that owns the bucket to upload into. Required with `GOOGLE_OIDC_CONFIG_FILE`.                                                                                                                                               |
| CACHE_CONTROL           | Caching behavior along the request/reply chain. Valid options are `no-cache`, `no-store`, `max-age=<seconds>`, `s-maxage=<seconds> no-transform`, `public`, `private`.                                                                  |
| CONTENT_DISPOSITION     | Indicates if the object is to be displayed inline or or as an attachment.                                                                                                                                                               |
| CONTENT_ENCODING        | Content encodings that have been applied to the object.                                                                                                                                                                                 |
| CONTENT_LANGUAGE        | The language for the intended audience of the object.                                                                                                                                                                                   |
| CONTENT_TYPE            | The media type of the object.                                                                                                                                                                                                           |
| ACL                     | ACL for the object when the command is performed. Valid values are: `project-private`, `private`, `public-read`, `public-read-write`, `authenticated-read`, `bucket-owner-read`, `bucket-owner-full-control`.                           |
| STORAGE_CLASS           | Type of storage to use for the object. Valid options are `multi_regional`, `regional`, `nearline`, `coldline`.                                                                                                                          |
| EMPTY_BUCKET            | Turn on emptying bucket before deploy. Default: `false`. Caution: [do not use if too many files are present][delete objects in bulk].                                                                                                   |
| DEBUG                   | Turn on extra debug information. Default: `false`.                                                                                                                                                                                      |

_(*) = required variable._


## Examples

### Basic example:

```yaml
script:
  - pipe: atlassian/google-cloud-storage-deploy:2.2.0
    variables:
      KEY_FILE: $KEY_FILE
      BUCKET: 'my-bucket'
      SOURCE: '.'
```

### Advanced example: 

Deploy to [destination blob][destination blob]:
```yaml
script:
  - pipe: atlassian/google-cloud-storage-deploy:2.2.0
    variables:
      KEY_FILE: $KEY_FILE
      BUCKET: 'my-bucket'
      SOURCE: 'myFile.txt'
      BLOB_NAME_PREFIX: 'my-folder/'
```

Provide additional parameters:
```yaml
script:
  - pipe: atlassian/google-cloud-storage-deploy:2.2.0
    variables:
      KEY_FILE: $KEY_FILE
      BUCKET: 'my-bucket'
      SOURCE: 'myFile.txt'
      CACHE_CONTROL: 'max-age=30'
      CONTENT_DISPOSITION: 'attachment'
      ACL: 'public-read'
      STORAGE_CLASS: 'nearline'
```

Deploy with OpenID Connect (OIDC):
```yaml
- step:
    oidc: true
    script:
      - pipe: atlassian/google-cloud-storage-deploy:2.2.0
        variables:
          GOOGLE_OIDC_CONFIG_FILE: $GOOGLE_OIDC_CONFIG_FILE
          PROJECT: 'my-project'
          BUCKET: 'my-bucket'
          SOURCE: '.'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce


## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,google
[Google Cloud Storage]: https://cloud.google.com/storage?hl=en
[OIDC authentication]: https://support.atlassian.com/bitbucket-cloud/docs/integrate-pipelines-with-resource-servers-using-oidc/
[Google service account]: https://cloud.google.com/iam/docs/creating-managing-service-account-keys
[encode-string-to-base64]: https://support.atlassian.com/bitbucket-cloud/docs/use-multiple-ssh-keys-in-your-pipeline/
[destination blob]: https://cloud.google.com/storage/docs/objects#simulated-folders
[delete objects in bulk]: https://cloud.google.com/storage/docs/deleting-objects#delete-objects-in-bulk